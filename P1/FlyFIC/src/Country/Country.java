/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Country;

/**
 *
 * @author BreixoCF
 */
public class Country {
    private int codeCity;
    private String name;
    
    public int getCodeCity() {
        return codeCity;
    }

    public void setCodeCity(int codeCity) {
        this.codeCity = codeCity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country(int codeCity, String name) {
        this.codeCity = codeCity;
        this.name = name;
    }
}
