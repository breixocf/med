/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Plane;

/**
 *
 * @author BreixoCF
 */
public class Plane {
    private int codePlane;
    private String model;
    private final int capacity;

    public int getCodePlane() {
        return codePlane;
    }

    public void setCodePlane(int codePlane) {
        this.codePlane = codePlane;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    
    public int getCapacity() {
        return capacity;
    }
    
    public Plane(int codePlane, String model, int capacity) {
        this.codePlane = codePlane;
        this.model = model;
        this.capacity = capacity;
    }
}
