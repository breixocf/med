/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airport;

/** Esta clase define objetos que contienen informaci�n referente a un aeropuerto.
 *
 *  @author  Breixo Cami�a Fern�ndez
 *  @author  El�as Ferreiro Borreiros
 *  @version 23/09/2015
 */
public class Airport {

    /** Atributos de la clase */
    private int codeAirport;
    private String name;

    public int getCodeAirport() {
        return codeAirport;
    }

    public void setCodeAirport(int codeAirport) {
        this.codeAirport = codeAirport;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /**
     * Constructor de la clase
     * @param  codeAirport  indica el c�digo de un vuelo para
     * @param  name the location of the image, relative to the url argument
     * @return      the image at the specified URL
     * @see         Image
     */
    public Airport(int codeAirport, String name) {
        this.codeAirport = codeAirport;
        this.name = name;
    }
}
