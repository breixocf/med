/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BoardingPass;

/**
 *
 * @author BreixoCF
 */
public class BoardingPass {
    private int codeBoardingPass;
    private String dniPassenger;
    private int codeSeat;

    public int getCodeBoardingPass() {
        return codeBoardingPass;
    }

    public void setCodeBoardingPass(int codeBoardingPass) {
        this.codeBoardingPass = codeBoardingPass;
    }

    public String getDniPassenger() {
        return dniPassenger;
    }

    public void setDniPassenger(String dniPassenger) {
        this.dniPassenger = dniPassenger;
    }

    public int getCodeSeat() {
        return codeSeat;
    }

    public void setCodeSeat(int codeSeat) {
        this.codeSeat = codeSeat;
    }

    public BoardingPass(int codeBoardingPass, String dniPassenger, int codePlane, int codeSeat) {
        this.codeBoardingPass = codeBoardingPass;
        this.dniPassenger = dniPassenger;
        this.codeSeat = codeSeat;
    }
}
