/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reserve;

import java.util.Calendar;

/**
 *
 * @author BreixoCF
 */
public class Reserve {
    private int codeReserve;
    private Calendar date;
    private double price;
    private String dniBuyer;

    public int getCodeReserve() {
        return codeReserve;
    }

    public void setCodeReserve(int codeReserve) {
        this.codeReserve = codeReserve;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDniBuyer() {
        return dniBuyer;
    }

    public void setDniBuyer(String dniBuyer) {
        this.dniBuyer = dniBuyer;
    }
    
    public Reserve(int codeReserve, Calendar date, double price, String dniBuyer) {
        this.codeReserve = codeReserve;
        this.date = date;
        this.price = price;
        this.dniBuyer = dniBuyer;
    }
}
