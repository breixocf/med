/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Seat;

/**
 *
 * @author BreixoCF
 */
public class Seat {
    
    private int codeSeat;
    private int row;
    private char letter;
    private int codePlane;
    private int codeFlight;

    public int getCodeSeat() {
        return codeSeat;
    }

    public void setCodeSeat(int codeSeat) {
        this.codeSeat = codeSeat;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public int getCodePlane() {
        return codePlane;
    }

    public void setCodePlane(int codePlane) {
        this.codePlane = codePlane;
    }

    public int getCodeFlight() {
        return codeFlight;
    }

    public void setCodeFlight(int codeFlight) {
        this.codeFlight = codeFlight;
    }

    public Seat(int codeSeat, int row, char letter, int codePlane, int codeFlight) {
        this.codeSeat = codeSeat;
        this.row = row;
        this.letter = letter;
        this.codePlane = codePlane;
        this.codeFlight = codeFlight;
    }
    
}
