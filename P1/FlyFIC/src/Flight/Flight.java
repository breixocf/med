/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Flight;

import java.util.Calendar;

/**
 *
 * @author BreixoCF
 */
public class Flight {
    private int codeFlight;
    private double price;
    private Calendar departureDate;
    private Calendar arrivalDate;
    private int occupiedSeats;

    public int getCodeFlight() {
        return codeFlight;
    }

    public void setCodeFlight(int codeFlight) {
        this.codeFlight = codeFlight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Calendar getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Calendar departureDate) {
        this.departureDate = departureDate;
    }

    public Calendar getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Calendar arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getOccupiedSeats() {
        return occupiedSeats;
    }

    public void addOccupiedSeats(int occupiedSeats){
        this.occupiedSeats += occupiedSeats;
    }
    
    public Flight(int codeFlight, double price, Calendar departureDate, Calendar arrivalDate) {
        this.codeFlight = codeFlight;
        this.price = price;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.occupiedSeats = 0;
    }
    
    
}
